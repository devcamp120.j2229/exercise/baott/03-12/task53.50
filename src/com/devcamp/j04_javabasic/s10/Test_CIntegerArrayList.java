package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class Test_CIntegerArrayList {
    public static void main(String[] args) {
        CIntegerArrayList array = new CIntegerArrayList();
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(25);
        arrayList.add(13);
        arrayList.add(35);
        arrayList.add(54);
        arrayList.add(56);
        arrayList.add(45);
        arrayList.add(58);
        array.setmIntegerArrayList(arrayList);
        System.out.println(array.getSum());

    }
}
