package com.devcamp.j04_javabasic.s10;

import com.devcamp.j04_javabasic.s10.interfaceclass.ISwimable;

public class CFish extends CPet implements ISwimable {

    @Override
    public void swim() {
        System.out.println("fish swimming");
        // TODO Auto-generated method stub

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        CPet myFish = new CFish();
        myFish.name = "Gold Fish";
        myFish.animclass = AnimalClass.fish;
        myFish.eat();
        myFish.animalSound();
        myFish.print();
        myFish.play();
        ((CFish) myFish).swim();

    }

}
